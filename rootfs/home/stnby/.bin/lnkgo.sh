#!/bin/dash
mpv $(curl -s "https://lnk.lt/api/main/video-page/`echo $1 | cut -d/ -f5,6`/false" \
    | jq -j '.videoConfig.videoInfo | "https://vod.lnk.lt/lnk_vod/lnk/lnk/", (.videoUrl | split(".")[-1]), ":", .videoUrl, "/playlist.m3u8", .secureTokenParams, "\n"')
