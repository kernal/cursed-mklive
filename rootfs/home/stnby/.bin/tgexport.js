#!/usr/bin/env node
'use strict';
const fs = require("fs");
let contents = fs.readFileSync(process.argv[2]);
let data = JSON.parse(contents);

data.forEach(function (message) {
    if (message.type == "message" && message.from == "İrem") {
        if (typeof(message.text) == "string") {
            console.log(message.text);
        }
        else {
            let text = "";
            message.text.forEach(function (element) {
                if (typeof(element) == "string") {
                    text += element;
                }
                else {
                    switch (element.type) {
                        case "link":
                            text += element.text;
                            break;
                        case "unknown":
                            text += element.text;
                            break;
                        case "mention":
                            text += element.text;
                            break;
                        case "hashtag":
                            text += element.text;
                            break;
                        case "bot_command":
                            text += element.text;
                            break;
                        case "url":
                            text += element.text;
                            break;
                        case "email":
                            text += element.text;
                            break;
                        case "bold":
                            text += element.text;
                            break;
                        case "italic":
                            text += element.text;
                            break;
                        case "code":
                            text += element.text;
                            break;
                        case "pre":
                            text += element.text;
                            break;
                        case "texturl": // idk
                            text += element.text;
                            break;
                        case "mentionname": // idk
                            text += element.text;
                            break;
                        case "phone":
                            text += element.text;
                            break;
                        case "cashtag":
                            text += element.text;
                            break;
                        case "underline":
                            text += element.text;
                            break;
                        case "strike":
                            text += element.text;
                            break;
                        case "blockquote":
                            text += element.text;
                            break;
                        case "bankcard": // idk
                            text += element.text;
                            break;
                    }
                }
            })
            console.log(text);
        }
    }
})
