#!/bin/sh
while read l; do
  curl --max-time 2 "http://$l/" | grep -q 'audio_in'
  [ $? -eq 0 ] && echo "$l" >> camswithaudio.txt
done <cams.txt
