#!/bin/dash
HOURS=`date +'%H'`

if [ `date +'%M'` -gt 30 ]
then
    curl -s "https://api.meteo.lt/v1/places/$1/forecasts/long-term" \
        | jq -r ".forecastTimestamps | .[ [.[].forecastTimeUtc] | index(\"`date '+%Y-%m-%d'` $HOURS:00:00\") + 1 ]"
else
    curl -s "https://api.meteo.lt/v1/places/$1/forecasts/long-term" \
        | jq -r ".forecastTimestamps | .[] | select(.forecastTimeUtc == \"`date '+%Y-%m-%d'` $HOURS:00:00\")"
fi
