#!/bin/dash
curl -s "https://corona.lmao.ninja/v2/countries/$1" \
    | jq -j '.active, " — ", .cases, " ", .deaths, " ", .recovered, "\n"'
