# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

PS1='\[\033[01;32m\]\u@\h\[\033[01;34m\] \w \$\[\033[00m\] '

#PS1='\[\033[01;31m\][\[\e[m\]\[\e[01;33m\]\u\[\e[m\]\[\e[01;32m\]@\[\e[m\]\[\e[01;34m\]\h\[\e[m\] \[\e[01;34m\]\W\[\e[m\]\[\e[01;31m\]]\[\e[m\]\\$ '
