#!/bin/dash
DBUS_USER=$(ps -o user --no-headers `pidof dbus-launch`)

case $1 in
    button/mute)
        #logger 'MuteButton pressed'
        su $DBUS_USER -c 'pamixer -t'
        ;;
    button/volumedown)
        #logger 'VolDownButton pressed'
        su $DBUS_USER -c 'pamixer -d 5'
        ;;
    button/volumeup)
        #logger 'VolUpButton pressed'
	su $DBUS_USER -c 'pamixer -i 5'
        ;;
    button/f20)
        #logger 'MicMuteButton pressed' 
	su $DBUS_USER -c 'pamixer --default-source -t'
        ;;
    video/brightnessdown)
        #logger 'BrightnessDownButton pressed'
        light -U 10
        ;;
    video/brightnessup)
        #logger 'BrightnessUpButton pressed'
        light -A 10
        ;;
    video/switchmode)
        logger 'VideoSwitchButton pressed'
        ;;
    button/wlan)
        logger 'WlanButton pressed'
        ;;
    ibm/hotkey)
        case $4 in
            00001314)
                logger 'BluetoothButton pressed'
                ;;
            00001315)
                logger 'KeyboardButton pressed'
                ;;
            00001311)
                logger 'StarButton pressed'
                ;;
            00001312)
                logger 'CutButton pressed'
                ;;
        esac
        ;;
    button/power)
        logger 'PowerButton pressed'
        poweroff
        ;;
    ac_adapter)
        case $4 in
            00000000)
                logger 'AC unpluged'
                ;;
            00000001)
                logger 'AC pluged'
                ;;
        esac
        ;;
    button/lid)
        case $3 in
            close)
                logger 'LID closed'
		zzz
                ;;
            open)
                logger 'LID opened'
                ;;
    esac
    ;;
    *)
        logger "ACPI undefined: $@"
        ;;
esac
#logger "ACPI test: $@"
