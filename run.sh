#!/bin/sh
PACKAGES="opendoas curl wget zsh zsh-completions bash-completion vim bind jq nmap htop exa bat fff rlwrap p7zip ncdu tor \
xorg-server xorg-input-drivers xorg-video-drivers xorg-fonts xinit xrdb setxkbmap xrandr \
light wpa_gui pulseaudio pamixer pulsemixer \
bspwm sxhkd rofi conky flameshot xterm alacritty mpv feh firefox mupdf neofetch youtube-dl"
./build.sh -K -T "Cursed Linux" -p "$PACKAGES" -I rootfs
